import pygame
import math
import numpy as np
import skfuzzy as fuzz
from skfuzzy import control as ctrl
import pygame
import sys
import time
import os

# Define inputs
speed = ctrl.Antecedent(np.arange(0, 101, 1), 'speed')
remainingDistance = ctrl.Antecedent(np.arange(0, 501, 1), 'remainingDistance')

# Define output
accelDecel = ctrl.Consequent(np.arange(-51, 51, 1), 'accelDecel')

# Define fuzzy sets for speed
speed['slow'] = fuzz.trimf(speed.universe, [0, 0, 35])
speed['medium'] = fuzz.trimf(speed.universe, [0, 35, 65])
speed['fast'] = fuzz.trimf(speed.universe, [35, 65, 100])

# "" for remainingDistance
remainingDistance['close'] = fuzz.trimf(remainingDistance.universe, [0, 0, 100])
remainingDistance['medium'] = fuzz.trimf(remainingDistance.universe, [0, 100, 250])
remainingDistance['far'] = fuzz.trimf(remainingDistance.universe, [100, 250, 500])

# Define fuzzy sets for acceleration and deceleration
accelDecel['brake'] = fuzz.trimf(accelDecel.universe, [0, 0, 15 ])
accelDecel['medium'] = fuzz.trimf(accelDecel.universe, [0, 15, 35])
accelDecel['high'] = fuzz.trimf(accelDecel.universe, [15, 35, 50])

# Define fuzzy rules
rule1 = ctrl.Rule(speed['slow'] & remainingDistance['close'], accelDecel['brake'])
rule2 = ctrl.Rule(speed['slow'] & remainingDistance['medium'], accelDecel['brake'])
rule3 = ctrl.Rule(speed['slow'] & remainingDistance['far'], accelDecel['brake'])

rule4 = ctrl.Rule(speed['medium'] & remainingDistance['close'], accelDecel['high'])
rule5 = ctrl.Rule(speed['medium'] & remainingDistance['medium'], accelDecel['medium'])
rule6 = ctrl.Rule(speed['medium'] & remainingDistance['far'], accelDecel['medium'])

rule7 = ctrl.Rule(speed['fast'] & remainingDistance['close'], accelDecel['high'])
rule8 = ctrl.Rule(speed['fast'] & remainingDistance['medium'], accelDecel['high'])
rule9 = ctrl.Rule(speed['fast'] & remainingDistance['far'], accelDecel['medium'])


# Create a fuzzy control system
angle_ctrl = ctrl.ControlSystem([rule1, rule2, rule3, rule4, rule5, rule6, rule7, rule8, rule9])

# Create a simulation with the fuzzy control system
accelDecel_simulation = ctrl.ControlSystemSimulation(angle_ctrl)

# Set the input temperature and compute the fuzzy output
speed_input = 50
distance_input = 100
initial_Distance = 100
accelDecel_simulation.input['speed'] = speed_input
accelDecel_simulation.input['remainingDistance'] = distance_input
accelDecel_simulation.compute()

# Get the crisp output from the fuzzy output
angle_output = accelDecel_simulation.output['accelDecel']

print('Fuzz outp:', accelDecel_simulation.output)



#--------------------------------



# Initialize Pygame
pygame.init()

# Set up the window
window_width = 1500
window_height = 600
window = pygame.display.set_mode((window_width, window_height))
pygame.display.set_caption("Moving Train")

# Set up the train
train_imageIni = pygame.image.load("train.png")
train_image = pygame.transform.scale(train_imageIni, (240, 50))
train_width = train_image.get_width()
train_height = train_image.get_height()
train_x = 0
train_y = window_height // 2 - train_height // 2

#set up destination
destination_imageIni = pygame.image.load("destination.png")
destination_image = pygame.transform.scale(destination_imageIni, (100,100))
destination_width = destination_image.get_width()
destination_height = destination_image.get_height()
#destination_x = 999 + initial_Distance
#destination_y = window_height // 2 - destination_height // 2


# Set up the clock
clock = pygame.time.Clock()

# Main game loop
while True:

    # Handle events
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

    for milliseconds in range(200):
        if speed_input > 0:
            if distance_input > 0:
                accelDecel_simulation.input['speed'] = speed_input
                accelDecel_simulation.input['remainingDistance'] = distance_input
                accelDecel_simulation.compute()
                print('Fuzz outp:', accelDecel_simulation.output)
                # km/h -> km/ms
                distance_input -= (speed_input / 360)
                speed_input -= (angle_output / 720)
                train_x += (speed_input / 5)


        print("time in seconds:", time.perf_counter())
        print("distance:", distance_input)
        print("speed:", speed_input)

        # check if the train has reached the right edge of the window
        if train_x > window_width:
            train_x = -train_width

        # Clear the window
        window.fill((255, 255, 255))

        # Draw the train
        window.blit(train_image, (train_x, train_y))
        if distance_input < 0:
            window.blit(destination_image, (train_x + 250, train_y -50))

        # Update the display
        pygame.display.flip()

        # Limit the frame rate
        clock.tick(60)






# Bootcamp23 Python Implementation

## Python
Für eine Einführung in Python gibt es hier ein von mir zusammengestelltes Gitlab https://gitlab.com/cediba24/python-introduction


## Fuzzy Logik:

"Fuzzy" beschreibt einen nicht-klaren oder unbestimmten Zustand. Anders zu der Booleschen Logik gibt es kein klares true und false, schwarz und weiß oder 0 und 1. Hier gibt es auch Zahlen dazwischen und grautöne zwischen Schwarz und weiß. Diese werden partielle Wahrheitswerte genannt.
In der Fuzzy-Logik werden unscharfe Begriffe verwendet, die eine graduellere Bewertung von Zuständen ermöglichen. Anstatt nur "wahr" oder "falsch" zu sein, können Aussagen in der Fuzzy-Logik beispielsweise "sehr wahr", "teilweise wahr" oder "weniger wahr" sein. Dies ermöglicht eine flexiblere Darstellung von menschlichem Denken und Entscheidungsprozessen.

Das grundlegende Konzept der Fuzzy-Logik besteht aus drei Hauptkomponenten: der Fuzzy-Menge, der Fuzzy-Regelbasis und dem Fuzzy-Inferenzsystem. Eine Fuzzy-Menge ist eine mathematische Darstellung unscharfer oder vager Begriffe. Sie besteht aus einer Menge von Werten und einer Zugehörigkeitsfunktion, die angibt, wie stark jedes Element zur Menge gehört.

Die Fuzzy-Regelbasis definiert die Beziehungen zwischen Eingangsvariablen und Ausgangsvariablen. Jede Regel besteht aus einer Bedingung und einer Konsequenz. Die Bedingung verwendet unscharfe Begriffe, um die Eingangsvariablen zu beschreiben, während die Konsequenz unscharfe Begriffe verwendet, um die Ausgangsvariablen zu bestimmen.

Das Fuzzy-Inferenzsystem kombiniert die Fuzzy-Regeln und Eingangswerte, um unscharfe Ausgangswerte zu berechnen. Dabei werden Methoden wie die Max-Min-Komposition oder die Mamdani-Methode verwendet, um die Auswirkungen der Regeln zu aggregieren und den unscharfen Ausgangswert zu bestimmen.

Die Anwendungsbereiche der Fuzzy-Logik sind vielfältig. Sie findet beispielsweise Anwendung in der Regelungstechnik, um Systeme zu steuern, bei denen die Eingänge oder Ausgänge unscharfe Werte aufweisen können. Auch in der künstlichen Intelligenz und in Expertensystemen wird die Fuzzy-Logik eingesetzt, um unscharfes Wissen zu modellieren und Entscheidungen zu treffen.

Die Fuzzy-Logik bietet einen flexiblen Ansatz zur Modellierung und Behandlung von Unsicherheit und vagen Konzepten. Durch die Verwendung von unscharfen Begriffen ermöglicht sie eine genauere Erfassung der menschlichen Denkweise und ermöglicht es, komplexe Probleme zu modellieren, für die herkömmliche binäre Logik nicht ausreichend ist.

<img src="images/fuzzyDiagram.png" alt="Alt text" width="25%" height= "auto" title="Optional title">

Fuzzy-Logik basiert auf einer mathematischen Berechnung und kann durch Graphen verstanden werden. In dem Diagramm kann man zwei Inputs und ein Output erkennen. Beschrieben wird eine Situation in einem Restaurant, wo abhängig von der Qualität des Essens und der Servicequalität das Trinkgeld bestimmt wird. Die verschiedenen Farben in dem Diagramm stellen die jeweiligen Inputs und Outputs dar. Der grüne Graph als Beispiel hat drei Echpunkte und bildet somit ein Dreieck. Diese drei Eckpunkte markieren Bereich für den jeweiligen Wert. Dies kann auf die anderen Farben und auch auf den Output übertragen werden. 


<img src="images/fuzzyDiagramOutput.png" alt="Alt text" width="25%" height= "auto" title="Fuzzy Output">

Nachdem der Output bestimmt wird mit Werten zwischen 0 und 1 entsprechend der drei verschiedenen Mengen an Trinkgeld, soll daraus ein Eurowert bestimmt werden. Vorgegangen wird dann folgendermaßen: Wenn 0.6 für "Low" bestimmt wird und 0.4 für "Medium", muss an den folgenden Stellen das Dreieck begrenzt werden (wie im Diagramm zu sehen). Die markierte Fläche stellt den Output dar und aus dieser Fläche muss dann der Mittelpunkt berechnet werden, um den Output zu erhalten. Der rote Punkt stellt dar, wo dieser zum Beispiel sein könnte.




# Anleitung zur Verwendung der Python-Bibliothek "skfuzzy" für Fuzzy-Logik

Die "skfuzzy" Bibliothek ist ein leistungsstarkes Werkzeug zur Implementierung von Fuzzy-Logik-Systemen in Python. Sie bietet eine umfangreiche Sammlung von Funktionen und Methoden zur Definition von unscharfen Variablen, unscharfen Mengen und unscharfen Regeln. In dieser Anleitung erfahren Sie, wie Sie die "skfuzzy" Bibliothek verwenden können, um Fuzzy-Logik in Ihren Python-Projekten einzusetzen.



Schritt 1: Installation

Stellen Sie sicher, dass Sie die "skfuzzy" Bibliothek auf Ihrem System installiert haben. Sie können dies einfach über den Paketmanager "pip" tun, indem Sie den folgenden Befehl ausführen:
```bash
pip install scikit-fuzzy
```

Schritt 2: Importieren der Bibliothek

Importieren Sie die "skfuzzy" Bibliothek in Ihr Python-Skript, um auf ihre Funktionen zugreifen zu können:
```python
import skfuzzy as fuzz
from skfuzzy import control as ctrl
```

Schritt 3: Definition der unscharfen Variablen und Mengen

Definieren Sie die unscharfen Variablen und Mengen, die Sie in Ihrem Fuzzy-Logik-System verwenden möchten. Verwenden Sie die Funktionen von "skfuzzy" wie z.B. `ctrl.Antecedent()` und `ctrl.Consequent()` zur Definition der Eingabe- und Ausgabemengen.
```python
# Beispiel: Definition einer unscharfen Eingabevariable "temperatur"
temperatur = ctrl.Antecedent(np.arange(0, 101, 1), 'temperatur')
temperatur['kalt'] = fuzz.trimf(temperatur.universe, [0, 0, 50])
temperatur['warm'] = fuzz.trimf(temperatur.universe, [0, 50, 100])

# Beispiel: Definition einer unscharfen Ausgabevariable "windstärke"
windstärke = ctrl.Consequent(np.arange(0, 101, 1), 'windstärke')
windstärke['schwach'] = fuzz.trimf(windstärke.universe, [0, 0, 50])
windstärke['stark'] = fuzz.trimf(windstärke.universe, [0, 50, 100])
```

Schritt 4: Definition der unscharfen Regeln

Definieren Sie die unscharfen Regeln, die die Beziehung zwischen den unscharfen Eingabevariablen und der unscharfen Ausgabevariablen beschreiben. Verwenden Sie die Funktion `ctrl.Rule()` zur Definition einzelner Regeln und kombinieren Sie diese zu einer Regelbasis.
```python
# Beispiel: Definition einer unscharfen Regel
regel1 = ctrl.Rule(temperatur['kalt'], windstärke['schwach'])
regel2 = ctrl.Rule(temperatur['warm'], windstärke['stark'])
regelbasis = ctrl.ControlSystem([regel1, regel2])
```

Schritt 5: Erstellen der Fuzzy-Logik-Simulation

Erstellen Sie eine Simulation mit dem erstellten Fuzzy-Logik-System. Verwenden Sie die Funktion `ctrl.ControlSystemSimulation()` und weisen Sie die unscharfen E

ingabewerte zu.
```python
simulation = ctrl.ControlSystemSimulation(regelbasis)
simulation.input['temperatur'] = 30
```

Schritt 6: Ausführen der Simulation und Ausgabe des Ergebnisses

Führen Sie die Simulation aus und erhalten Sie das unscharfe Ausgaberesultat. Verwenden Sie die Methode `compute()` und greifen Sie auf die unscharfe Ausgabe über `simulation.output['ausgabe']` zu.
```python
simulation.compute()
ergebnis = simulation.output['windstärke']
print("Unscharfes Ausgaberesultat: ", ergebnis)
```




# Pygame-Grundlagen

Schritt 1: Pygame installieren

Um Pygame zu verwenden, müssen Sie es zunächst installieren. Öffnen Sie die Kommandozeile und geben Sie den folgenden Befehl ein, um Pygame mit pip zu installieren:
```
pip install pygame
```

Schritt 2: Das Pygame-Fenster initialisieren

Importieren Sie das pygame-Modul und initialisieren Sie das Fenster, in dem das Spiel angezeigt wird:
```python
import pygame

pygame.init()
fenster = pygame.display.set_mode((Breite, Höhe))
pygame.display.set_caption("Mein Spiel")
```

Schritt 3: Die Hauptschleife einrichten

Richten Sie die Hauptschleife ein, die das Spiel ausführt und auf Benutzereingaben oder andere Ereignisse reagiert:
```python
spiel_läuft = True

while spiel_läuft:
    for ereignis in pygame.event.get():
        if ereignis.type == pygame.QUIT:
            spiel_läuft = False
    
    # Spiellogik und -aktualisierung hier einfügen
    
    fenster.fill(Hintergrundfarbe)
    
    # Objekte zeichnen und andere visuelle Effekte hier einfügen
    
    pygame.display.update()

pygame.quit()
```

Schritt 4: Logik und Aktualisierung

Fügen Sie Ihre Logik hinzu, wie z. B. die Bewegung von Objekten oder Kollisionserkennung, innerhalb der Hauptschleife ein.

Schritt 5: Zeichnen von Objekten und visuellen Effekten

Verwenden Sie Pygame-Funktionen, um Objekte zu zeichnen und visuelle Effekte darzustellen. Dazu gehören Funktionen wie `pygame.draw.rect()`, `pygame.draw.circle()`, `pygame.draw.line()`, `pygame.image.load()` und viele mehr. Sie können auch Bilder, Musik und Soundeffekte in Ihr Spiel einbinden.

Schritt 6: Benutzereingaben verarbeiten

Reagieren Sie auf Benutzereingaben, wie Tastatureingaben oder Mausklicks, um das Verhalten Ihres Spiels zu steuern. Verwenden Sie die Funktionen `pygame.key.get_pressed()` und `pygame.mouse.get_pos()` zum Erfassen von Benutzereingaben.

Das sind die grundlegenden Schritte, um mit Pygame zu starten. Es gibt jedoch noch viele weitere Funktionen und Konzepte, die Sie erkunden können, um Ihr Spiel zu erweitern. Sie können die offizielle Pygame-Dokumentation (https://www.pygame.org/docs/) besuchen, um weitere Informationen zu erhalten und Beispiele anzusehen.
